package com.strong.demo;

import com.jfinal.plugin.activerecord.Model;

public class DemoDao extends Model<DemoDao> {
    private static final long serialVersionUID = 1L;

    public final static DemoDao dao = new DemoDao();

    public String getOne(){
        return dao.findById(4).get("DEMO_NAME");
    }

}
