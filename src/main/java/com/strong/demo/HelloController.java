package com.strong.demo;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;

import java.io.BufferedReader;


public class HelloController extends Controller {


    public void index() {

        DemoReq req = new DemoReq();

        try {
            req = getRequestObject(DemoReq.class);
        } catch (Exception e) {
            e.printStackTrace();
            DemoResp resp = new DemoResp();
            resp.setRetCode("9999");
            resp.setRetMsg("傻逼");
            renderJson(resp);
            return;
        }

        String paramStr = req.getParaStr();


        DemoResp resp = new DemoResp();
        resp.setRetCode("0000");
        resp.setRetMsg(paramStr);
        renderJson(resp);

        //renderText("Hello JFinal World.");
    }


    public void test() {
        renderJsp("/index.jsp");
    }

    /**
     * 取Request中的数据对象
     *
     * @param valueType
     * @return
     * @throws Exception
     */
    protected <T> T getRequestObject(Class<T> valueType) throws Exception {
        StringBuilder json = new StringBuilder();
        BufferedReader reader = this.getRequest().getReader();
        String line = null;
        while ((line = reader.readLine()) != null) {
            json.append(line);
        }
        reader.close();
        return JSONObject.parseObject(json.toString(), valueType);
    }


    public void login(){
        DemoDao demo = getModel(DemoDao.class);
        renderText(demo.findById(4).get("DEMO_NAME").toString());

    }

}
