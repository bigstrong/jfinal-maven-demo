package com.strong.demo;

/**
 * demo请求对象
 */
public class DemoReq {

    private String paraStr;

    public String getParaStr() {
        return paraStr;
    }

    public void setParaStr(String paraStr) {
        this.paraStr = paraStr;
    }

    @Override
    public String toString() {
        return "DemoReq{" +
                "paraStr='" + paraStr + '\'' +
                '}';
    }
}
